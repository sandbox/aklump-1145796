/* $Id$ */

SUMMARY: Replaces the pattern node/123 with it's alias if available.


REQUIREMENTS:


INSTALLATION:
* Download and unzip this module into your modules directory.
* Goto Administer > Site Building > Modules and enable this module.


CONFIGURATION:
* It's a good idea to place this filter AFTER the 'URL filter'


USAGE:
*


API:
*


--------------------------------------------------------
CONTACT:
In the Loft Studios
Aaron Klump - Web Developer
PO Box 29294 Bellingham, WA 98228-1294
aim: theloft101
skype: intheloftstudios

http://www.InTheLoftStudios.com